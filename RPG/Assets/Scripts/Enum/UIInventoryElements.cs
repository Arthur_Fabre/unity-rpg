﻿using UnityEngine;
using System.Collections;

public enum UIInventoryElements
{
    Sword,
    Shield,
    Bomb,
    Bow,
    Exit,
    Save
}

public enum UIItemElements
{
    Sword,
    Shield,
    Bomb,
    Bow,
    Empty
}

public enum UIKeyElements
{
    Action1,
    Action2,
    Action3
}

public enum UIKeyAction3
{
    Run,
    Interact,
    Speak,
    Quit
}
