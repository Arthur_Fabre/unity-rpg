﻿using UnityEngine;
using System.Collections;

public class Utils
{

    public static void killEntity(GameObject gameObject)
    {
        gameObject.SetActive(false);
        gameObject.GetComponent<BoxCollider2D>().enabled = false;
    }

    public static void RespawnEntityObject(GameObject gameObject)
    {
        gameObject.SetActive(true);
        gameObject.GetComponent<BoxCollider2D>().enabled = true;
    }
}
