﻿using UnityEngine;
using System.Collections;

public class DEBUG : MonoBehaviour
{

    public PlayerData player;
    public NpcData npc1;
    public NpcData mob1;

    // Use this for initialization
    void Awake()
    {
        player.Life = 20;
        npc1.Life = 3;
        mob1.Life = 3;
    }

}
