﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class PlayerInventory : ScriptableObject
{
    public bool SwordUnlock;
    public bool ShieldUnlock;
    public bool BombUnlock;
    public bool BowUnlock;

    public UIItemElements Action1Equipment;
    public UIItemElements Action2Equipment;
    public UIKeyAction3 Action3Interact;
    public int Money;

    public void SetData(PlayerInventory newPlayerInventory)
    {
        SwordUnlock = newPlayerInventory.SwordUnlock;
        ShieldUnlock = newPlayerInventory.ShieldUnlock;
        BombUnlock = newPlayerInventory.BombUnlock;
        BowUnlock = newPlayerInventory.BowUnlock;
        Action1Equipment = newPlayerInventory.Action1Equipment;
        Action2Equipment = newPlayerInventory.Action2Equipment;
        Action3Interact = newPlayerInventory.Action3Interact;
        Money = newPlayerInventory.Money;
    }
}
