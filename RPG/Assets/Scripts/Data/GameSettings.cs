﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class GameSettings : ScriptableObject
{
    [Header("Interface type")]
    public bool mobile;
    [Header("Sound setings")]
    public bool PlayAmbientSound;
    public bool PlayInteractSound;
    [Range(0, 1)]
    public float VolumeAmbientSound;
    [Range(0, 1)]
    public float VolumeInteractSound;
    [Header("Game state")]
    public bool standby;
    public bool save;
}
