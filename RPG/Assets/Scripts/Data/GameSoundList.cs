﻿using UnityEngine;
using UnityEditor;

[CreateAssetMenu]
public class GameSoundList : ScriptableObject
{

    [Header("Menu")]
    public AudioClip Select;
    public AudioClip Change;
    public AudioClip InventoryOpen;
    public AudioClip GameOverOff;
    public AudioClip TitleScreenOff;

    [Header("Player")]
    public AudioClip Walk;
    public AudioClip Attack;
    public AudioClip Protect;
}

