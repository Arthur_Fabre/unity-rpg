﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class PlayerData : ScriptableObject
{
    public string PlayerName;
    public int Life;
    public int MaxLife;
    public bool IsProtected;
    public Vector3 position;
    public PlayerInventory Inventory;
    public PlaceData Place;

    public void SetData(PlayerData newPlayerData)
    {
        PlayerName = newPlayerData.PlayerName;
        Life = newPlayerData.Life;
        MaxLife = newPlayerData.MaxLife;
        position = newPlayerData.position;
        Place = newPlayerData.Place;
        Inventory.SetData(newPlayerData.Inventory);
    }

    public void AddMoney()
    {
        Inventory.Money++;
    }

    public void AddMaxLife()
    {
        MaxLife++;
    }

    public void AddLife()
    {
        if(Life+1 <= MaxLife) Life++;
    }

    public void HitPlayer(int Damage)
    {
        if(!IsProtected)
            Life -= Damage;
    }

    public int GetAtk()
    {
        return 1;
    }

    
}
