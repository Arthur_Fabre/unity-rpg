﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class NpcData : ScriptableObject
{
    public NpcData BaseNpc;
    public string NpcName;
    public int Life;
    public bool IsHostile;
    public bool IsMoving;
    public bool IsFollowing;
    public bool IsInvincible;
    public bool IsRespawnable;
    public Vector3 InitPosition;

    public void SetData()
    {
        NpcName = BaseNpc.NpcName;
        Life = BaseNpc.Life;
        IsHostile = BaseNpc.IsHostile;
        IsFollowing = BaseNpc.IsFollowing;
        IsMoving = BaseNpc.IsMoving;
        IsInvincible = BaseNpc.IsInvincible;
    }


    public void HitNpc(int Damage)
    {
        Life -= Damage;
    }
}
