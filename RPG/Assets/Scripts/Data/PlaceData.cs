﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu]
public class PlaceData : ScriptableObject
{
    public string Text = "";
    public AudioClip PlaceSound;

}
