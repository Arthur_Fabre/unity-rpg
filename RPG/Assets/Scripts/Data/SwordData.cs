﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class SwordData : ScriptableObject
{
    public int Atk = 1;
}
