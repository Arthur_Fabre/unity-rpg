﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class DialogueData : ScriptableObject
{
    [TextArea]
    public string m_Text = "";
    [TextArea]
    public string[] m_DialogList;
    public Sprite m_Sprite = null;
}
