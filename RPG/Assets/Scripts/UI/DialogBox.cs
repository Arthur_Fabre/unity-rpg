﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    public Image m_Avatar = null;
    public Text m_Text = null;
    [SerializeField] bool m_IsMulti = false;
    [SerializeField] Image m_Next;
    private bool _IsActive = false;
    private DialogueData _dialogue;
    private int _CurrentDialog;

    private void Awake()
    {
        CloseDialog();
        _IsActive = false;
    }

    private void Update()
    {
        if (_IsActive && _dialogue.m_DialogList.Length > 1)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (_CurrentDialog < _dialogue.m_DialogList.Length)
                {
                    _CurrentDialog++;
                    UpdateNextText();
                }
                else
                {
                    CloseDialog();
                }

            }
            
        }
    }

    public void DisplayDialog(DialogueData dialog)
    {
        Debug.Log("DisplayDialog");
        _dialogue = dialog;
        _CurrentDialog = 0;
        m_Avatar.sprite = _dialogue.m_Sprite;
        UpdateNextText();
        Debug.Log(gameObject.active);
        gameObject.SetActive(true);
        Debug.Log(gameObject.active);
        _IsActive = true;
    }

    public void CloseDialog()
    {
        gameObject.SetActive(false);
        _IsActive = false;
    }

    public bool GetActive()
    {
        return _IsActive;
    }

    private void UpdateNextText()
    {
        m_IsMulti = _CurrentDialog + 1 < _dialogue.m_DialogList.Length;
        m_Text.text = _dialogue.m_DialogList[_CurrentDialog];
        m_Next.gameObject.SetActive(m_IsMulti);
    }
}
