﻿using UnityEngine;
using System.Collections;

public class EnableMobile : MonoBehaviour
{
    [SerializeField]
    private GameSettings m_GameSettings;

    private void Awake()
    {
        gameObject.SetActive(m_GameSettings.mobile);
    }
}
