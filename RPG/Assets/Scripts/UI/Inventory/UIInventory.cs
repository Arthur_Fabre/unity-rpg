﻿using UnityEngine;
using System.Collections;

public class UIInventory : MonoBehaviour
{
    [SerializeField] private GameObject m_SelectorItemImage;
    [SerializeField] private GameObject m_SwordImage;
    [SerializeField] private GameObject m_ShieldImage;
    [SerializeField] private GameObject m_BombImage;
    [SerializeField] private GameObject m_BowImage;
    [SerializeField] private GameObject m_SaveImage;
    [SerializeField] private GameObject m_ExitImage;
    [SerializeField] private PlayerData m_PlayerData;
    [SerializeField] private PlayerData m_PlayerDataSave;
    [SerializeField] private AudioSource m_AudioSource;
    [SerializeField] private GameSoundList m_GameSound;
    [SerializeField] private GameSettings m_GameSettings;


    private int _Input = 0;

    public void Awake()
    {
        DisableElementSelector();
    }

    //0 = nothing
    //1 = W  ||5 = top
    //2 = X  ||6 = left
    //3 = C  ||7 = down
    //4 = V  ||8 = right
    public void Update()
    {
        IsElementDisplay();

        if (Input.GetKeyDown(KeyCode.W)) _Input = 1;
        else if (Input.GetKeyDown(KeyCode.X)) _Input = 2;
        else if (Input.GetKeyDown(KeyCode.C)) _Input = 3;
        else if (Input.GetKeyDown(KeyCode.V)) _Input = 4;
        else if (Input.GetKeyDown(KeyCode.UpArrow)) _Input = 5;
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) _Input = 6;
        else if (Input.GetKeyDown(KeyCode.DownArrow)) _Input = 7;
        else if (Input.GetKeyDown(KeyCode.RightArrow)) _Input = 8;
    }

    private void IsElementDisplay()
    {
        m_SwordImage.SetActive(m_PlayerData.Inventory.SwordUnlock);
        m_BombImage.SetActive(m_PlayerData.Inventory.BombUnlock);
        m_BowImage.SetActive(m_PlayerData.Inventory.BowUnlock);
        m_ShieldImage.SetActive(m_PlayerData.Inventory.ShieldUnlock);
    }


    public int GetInput()
    {
        return _Input;
    }

    public void DisableElementSelector()
    {
        _Input = 0;
        m_SelectorItemImage.SetActive(false);
    }

    public void SelectElement(UIInventoryElements Item)
    {
        if (Item == UIInventoryElements.Sword) SetImageSelector(m_SwordImage);
        else if (Item == UIInventoryElements.Shield) SetImageSelector(m_ShieldImage);
        else if (Item == UIInventoryElements.Bomb) SetImageSelector(m_BombImage);
        else if (Item == UIInventoryElements.Bow) SetImageSelector(m_BowImage);
        else if(Item == UIInventoryElements.Save) SetImageSelector(m_SaveImage);
        else if (Item == UIInventoryElements.Exit) SetImageSelector(m_ExitImage);
    }

    private void SetImageSelector(GameObject objectElement)
    {
        m_SelectorItemImage.transform.position = objectElement.transform.position;
        m_SelectorItemImage.GetComponent<RectTransform>().sizeDelta = objectElement.GetComponent<RectTransform>().sizeDelta;
        m_SelectorItemImage.SetActive(true);
    }

    public void QuitGame()
    {
        if(_Input == 1)
        {
           PlaySound(m_GameSound.Select);
            m_GameSettings.standby = false;
            Application.Quit();
        }
        _Input = 0;
    }

    public void SaveGame()
    {
        if (_Input == 1)
        {
            PlaySound(m_GameSound.Select);
            m_PlayerDataSave.SetData(m_PlayerData);
            m_GameSettings.save = true;
        }
        _Input = 0;
    }

    public void UseElement(UIItemElements Item)
    {
        if (IsUnlock(Item))
        {
            if (_Input == 1)
            {
                PlaySound(m_GameSound.Select);
                if (Item == m_PlayerData.Inventory.Action2Equipment)
                {
                    m_PlayerData.Inventory.Action2Equipment = m_PlayerData.Inventory.Action1Equipment;
                }
                m_PlayerData.Inventory.Action1Equipment = Item;
            }
            else if (_Input == 2)
            {
                PlaySound(m_GameSound.Select);
                if (Item == m_PlayerData.Inventory.Action1Equipment)
                {
                    m_PlayerData.Inventory.Action1Equipment = m_PlayerData.Inventory.Action2Equipment;
                }
                m_PlayerData.Inventory.Action2Equipment = Item;
            }
            _Input = 0;
        }
    }

    private bool IsUnlock(UIItemElements Item)
    {
        bool unlock = false;
        if (Item == UIItemElements.Sword) unlock = m_PlayerData.Inventory.SwordUnlock;
        else if (Item == UIItemElements.Shield) unlock = m_PlayerData.Inventory.ShieldUnlock;
        else if (Item == UIItemElements.Bomb) unlock = m_PlayerData.Inventory.BombUnlock;
        else if (Item == UIItemElements.Bow) unlock = m_PlayerData.Inventory.BowUnlock;

        return unlock;
    }

    public void ChangeState()
    {
        PlaySound(m_GameSound.Change);
    }

    private void PlaySound(AudioClip clip)
    {
        m_AudioSource.clip = clip;
        m_AudioSource.Play();
    }
}
