﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetEquipementDispaly : MonoBehaviour
{
    [SerializeField] private PlayerInventory m_PlayerInventory;
    [SerializeField] private Sprite M_SwordImage;
    [SerializeField] private Sprite M_ShieldImage;
    [SerializeField] private Sprite M_BombImage;
    [SerializeField] private Sprite M_BowImage;
    [SerializeField] private Sprite M_EmptyImage;
    [SerializeField] private GameObject m_KeyAction1;
    [SerializeField] private GameObject m_KeyAction2;
    [SerializeField] private Text m_KeyAction3;


    public void Update()
    {
        if (m_KeyAction1 != null) SetSprite(m_KeyAction1, m_PlayerInventory.Action1Equipment);
        if (m_KeyAction2 != null) SetSprite(m_KeyAction2, m_PlayerInventory.Action2Equipment);
        if (m_KeyAction3 != null) m_KeyAction3.text = m_PlayerInventory.Action3Interact.ToString();
    }

    private void SetSprite(GameObject KeyAction, UIItemElements EquipmentInKey)
    {
        if (EquipmentInKey == UIItemElements.Sword) KeyAction.GetComponent<Image>().sprite = M_SwordImage;
        else if (EquipmentInKey == UIItemElements.Shield) KeyAction.GetComponent<Image>().sprite = M_ShieldImage;
        else if (EquipmentInKey == UIItemElements.Bomb) KeyAction.GetComponent<Image>().sprite = M_BombImage;
        else if (EquipmentInKey == UIItemElements.Bow) KeyAction.GetComponent<Image>().sprite = M_BowImage;
        else if (EquipmentInKey == UIItemElements.Empty) KeyAction.GetComponent<Image>().sprite = M_EmptyImage;
    }
}
