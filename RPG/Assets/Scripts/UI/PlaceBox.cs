﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaceBox : MonoBehaviour
{
    public Text m_Text = null;
    private bool _IsActive = false;

    private void Awake()
    {
        CloseBox();
        _IsActive = false;
    }

    public void DisplayBox(PlaceData place)
    {
        m_Text.text = place.Text;
        gameObject.SetActive(true);
        _IsActive = true;
    }

    public void CloseBox()
    {
        gameObject.SetActive(false);
        _IsActive = false;
    }

    public bool GetActive()
    {
        return _IsActive;
    }
}

