﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{

    public Text m_MoneyCount;
    public Text m_LifeCount;
    public PlayerData _PlayerData;

    // Use this for initialization
    void Awake()
    {
        m_MoneyCount.text = _PlayerData.Inventory.Money.ToString();
        m_LifeCount.text = _PlayerData.Life.ToString() + "/" + _PlayerData.MaxLife.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        m_MoneyCount.text = _PlayerData.Inventory.Money.ToString();
        m_LifeCount.text = _PlayerData.Life.ToString() + "/" + _PlayerData.MaxLife.ToString();
    }
}
