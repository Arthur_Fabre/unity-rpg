﻿using UnityEngine;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject m_HudPause;
    [SerializeField]
    private GameObject m_GameHUD;
    [SerializeField]
    private GameSettings m_GameSettings;
    void Start()
    {
        m_HudPause.SetActive(false);
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (!m_HudPause.activeInHierarchy)
            {
                PauseGame();
            }
            else if (m_HudPause.activeInHierarchy)
            {
                ContinueGame();
            }
        }

        if (m_HudPause.activeInHierarchy && Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
    private void PauseGame()
    {
        //Debug.Log("pass");
        //Time.timeScale = 0;
        m_GameSettings.standby = true;
        m_HudPause.SetActive(true);
        m_GameHUD.SetActive(false);
        //Disable scripts that still work while timescale is set to 0
    }
    private void ContinueGame()
    {
        //Time.timeScale = 1;
        m_GameSettings.standby = false;
        m_HudPause.SetActive(false);
        m_GameHUD.SetActive(true);
        //enable the scripts again
    }
}
