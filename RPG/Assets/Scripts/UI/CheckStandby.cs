﻿using UnityEngine;
using System.Collections;

public class CheckStandby : MonoBehaviour
{
    [SerializeField]
    private GameSettings m_GameSettings;

    public bool GetGameStandby()
    {
        return m_GameSettings.standby;
    }
}
