﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public enum Scene { TitleScreen, TechnicalDemo}

public class SpaceListener : MonoBehaviour
{
    [SerializeField] private Scene ChoseScene;
    [SerializeField] private GameSettings m_GameSettings;
    [SerializeField] private PlayerData m_BasePlayer;
    [SerializeField] private PlayerData m_SavePlayer;
    [SerializeField] private PlayerData m_Player;
    [SerializeField] private GameObject m_resetButton;



    // Update is called once per frame
    private void Awake()
    {
        m_resetButton.SetActive(m_GameSettings.save);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ChoseScene == Scene.TechnicalDemo)
                SetPlayerData();
            SceneManager.LoadScene(ChoseScene.ToString());

        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            m_GameSettings.save = false;
            m_resetButton.SetActive(false);
        }
    }

    private void SetPlayerData()
    {
        if (m_GameSettings.save)
            m_Player.SetData(m_SavePlayer);
        else
            m_Player.SetData(m_BasePlayer);
    }
}
