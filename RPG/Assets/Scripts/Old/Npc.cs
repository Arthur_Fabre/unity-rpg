﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class Npc : Character
{
    public int m_MinimalChange = 50;
    public int m_MaximalChange = 100;
    public string m_TagMask = "Player";
    public bool m_IsMovingNpc = false;

    public DialogueData m_Dialogue = null;
    public DialogBox m_DialogBox = null;



    protected int _ChangeDirValue;
    protected int _CurentValue = 0;
    protected int _Dir;
    protected bool _DirChage = false;
    protected GameObject[] _Players = null;

    protected override void Awake()
    {
        _ChangeDirValue = Rand();
        _Dir = Random.Range(0, 5);
        _Animator = GetComponent<Animator>();
        _Players = GameObject.FindGameObjectsWithTag(m_TagMask);
        _directionAngle = 180;
        _refPoint = new Vector3(transform.position.x, transform.position.y - m_DetectionRange/2, transform.position.z);
        _refVector = VectorCoord(_refPoint, transform.position);
    }

    protected int Rand()
    {
        return Random.Range(m_MinimalChange, m_MaximalChange);
    }

    protected void ChangeDir()
    {
        _ChangeDirValue = Rand();
        _CurentValue = 0;
        _Dir = Random.Range(0, 5);
        _DirChage = true;
    }

    protected override void Move()
    {
        if (_CurentValue > _ChangeDirValue) ChangeDir();
        Vector3 mouvement = Vector3.zero;

        if (_Dir == 1 && _DirChage)
        {
            SetDirection("Down", 0, new Vector3(transform.position.x, transform.position.y + m_DetectionRange/2, transform.position.z));
            _IsWalking = true;
        }
        else if (_Dir == 3 && _DirChage)
        {
            SetDirection("Up", 180, new Vector3(transform.position.x, transform.position.y - m_DetectionRange/2, transform.position.z));
            _IsWalking = true;
        }
        else if (_Dir == 4 && _DirChage)
        {
            SetDirection("Left", 270, new Vector3(transform.position.x + m_DetectionRange/2, transform.position.y, transform.position.z));
            _IsWalking = true;
        }
        else if (_Dir == 2 && _DirChage)
        {
            SetDirection("Right", 90, new Vector3(transform.position.x - m_DetectionRange/2, transform.position.y, transform.position.z));
            _IsWalking = true;
        }
        else if (_Dir == 0 && _DirChage)
        {
            _IsWalking = false;
        }

        _DirChage = false;
        _Animator.SetBool("isWalking", _IsWalking);
        

        if (_Dir == 1) mouvement.y = -transform.position.y;
        else if (_Dir == 3) mouvement.y = transform.position.y;
        else if (_Dir == 2) mouvement.x = transform.position.x;
        else if (_Dir == 4) mouvement.x = -transform.position.x;


        mouvement.Normalize();
        _refPoint += mouvement * m_MoveSpeed * Time.deltaTime;
        _refVector = VectorCoord(_refPoint, transform.position);
        transform.position += mouvement * m_MoveSpeed * Time.deltaTime;
        _CurentValue++;
    }

    protected void MoveHit(int Dir)
    {
        Vector3 mouvement = Vector3.zero;

        if (Dir == 3) mouvement.y = -transform.position.y;
        else if (Dir == 1) mouvement.y = transform.position.y;
        else if (Dir == 4) mouvement.x = transform.position.x;
        else if (Dir == 2) mouvement.x = -transform.position.x;

        mouvement.Normalize();
        _refPoint += mouvement * 6 * Time.deltaTime;
        _refVector = VectorCoord(_refPoint, transform.position);
        transform.position += mouvement * 6 * Time.deltaTime;
    }
}
