﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mob : Npc
{

    public GameObject m_SummonItem;
    private bool _PlayerDetected = false;
    private Vector3 _PlayerPos = new Vector3(0,0,0);
    private bool _IsPlayerAtkRange = false;
    public float m_AtkSpeed;
    public int m_AtkDamage;
    private int _KnowBackDir = 0;

    private float _CurentAtkTimeValue = 0;
    private float _CurentCoolDownValue = 0;

    protected override void Update()
    {
        _PlayerDetected = false;
        _methodToCall = ActionEvent;
        DetectOnLayer(_Players, _methodToCall);
        if (m_IsMovingNpc)
        {
            if (_CurentCoolDownValue <= 0)
            {
                if (_PlayerDetected)
                {
                    if (!_IsPlayerAtkRange)
                    {
                        MoveToPlayer();
                    }
                    else
                    {
                        AtkPlayer();
                    }
                }
                else
                {
                    Move();
                }
            }
            else
            {
                MoveHit(_KnowBackDir);
                _CurentCoolDownValue--;
            }
        }
        Blink();
    }

    protected override void ActionEvent(GameObject objectElement)
    {
        if(objectElement.CompareTag("Player"))
        {
            _PlayerDetected = true;
            _PlayerPos = objectElement.transform.position;
        }
    }

    public void TakeDamage(int Damage, int angle)
    {
        _IsBlinking = true;
        m_CharacterLife -= Damage;
        if (angle == 0) _KnowBackDir=3;
        else if (angle == 90) _KnowBackDir=4;
        else if (angle == 180) _KnowBackDir=1;
        else if (angle == 270) _KnowBackDir=2;


        if (m_CharacterLife < 1)
        {
             m_SummonItem.transform.position = transform.position;
             Destroy(gameObject, 0.1f);
             Instantiate(m_SummonItem);
        }
        _CurentCoolDownValue = 20;
    }

    public void MoveToPlayer()
    {
        _refPoint = _PlayerPos;
        _refVector = VectorCoord(_refPoint, transform.position);
        float step = (m_MoveSpeed+2) * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _PlayerPos, step);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _IsPlayerAtkRange = true;
        }
    }


    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _IsPlayerAtkRange = false;
        }
    }

    private void AtkPlayer()
    {
        if (_CurentAtkTimeValue > m_AtkSpeed)
        {
             _Players[0].GetComponent<Player>().TakeDamage(m_AtkDamage, _directionAngle);
            _CurentAtkTimeValue = 0;
        }
        else
        {
            _CurentAtkTimeValue++;
        }
    }
}
