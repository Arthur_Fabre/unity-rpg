﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : Character
{
    private bool m_IsAtk;
    private GameObject[] m_Pots = null;
    private GameObject[] m_Enemies = null;
    private PlayerData _PlayerData = null;


    protected override void Awake()
    {
        //_PlayerData = PlayerData.getInstance();
        _Animator = GetComponent<Animator>();
        _directionAngle = 180;
        _refPoint = new Vector3(transform.position.x, transform.position.y - m_DetectionRange / 2, transform.position.z);
        _refVector = VectorCoord(_refPoint, transform.position);
    }

    protected override void Update()
    {
        m_Pots = GameObject.FindGameObjectsWithTag("Pot");
        m_Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        Move();

        if (Input.GetKeyDown(KeyCode.C))
        {
            m_IsAtk = true;
            //Pots
            _methodToCall = ActionEvent;
            DetectOnLayer(m_Pots, _methodToCall);
            //Enemy
            _methodToCall = ActionEventEnemy;
            DetectOnLayer(m_Enemies, _methodToCall);
        }
        else if (Input.GetKeyUp(KeyCode.C))
        {
            m_IsAtk = false;
        }
        _Animator.SetBool("isAtk", m_IsAtk);

        Blink();
    }

    protected override void Move()
    {
        Vector3 mouvement = Vector3.zero;
        mouvement.x = Input.GetAxisRaw("Horizontal");
        mouvement.y = Input.GetAxisRaw("Vertical");
        mouvement.Normalize();

        if (Input.GetKeyDown(KeyCode.DownArrow)) SetDirection("Down", 180, new Vector3(transform.position.x, transform.position.y - m_DetectionRange / 2, transform.position.z));
        else if (Input.GetKeyDown(KeyCode.UpArrow)) SetDirection("Up", 0, new Vector3(transform.position.x, transform.position.y + m_DetectionRange / 2, transform.position.z));
        else if (Input.GetKeyDown(KeyCode.LeftArrow)) SetDirection("Left", 90, new Vector3(transform.position.x - m_DetectionRange / 2, transform.position.y, transform.position.z));
        else if (Input.GetKeyDown(KeyCode.RightArrow)) SetDirection("Right", 270, new Vector3(transform.position.x + m_DetectionRange / 2, transform.position.y, transform.position.z));

        _refPoint += mouvement * m_MoveSpeed * Time.deltaTime;
        _refVector = VectorCoord(_refPoint, transform.position);

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            m_MoveSpeed += 2;
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            m_MoveSpeed -= 2;
        }


        _IsWalking = true;
        if (Input.GetKey(KeyCode.DownArrow)) { }
        else if (Input.GetKey(KeyCode.UpArrow)) { }
        else if (Input.GetKey(KeyCode.LeftArrow)) { }
        else if (Input.GetKey(KeyCode.RightArrow)) { }
        else _IsWalking = false;

        _Animator.SetBool("isWalking", _IsWalking);
        transform.position += mouvement * m_MoveSpeed * Time.deltaTime;
    }

    protected override void ActionEvent(GameObject objectElement)
    {
        objectElement.GetComponent<Animator>().SetTrigger("Broke");
    }

    protected void ActionEventEnemy(GameObject objectElement)
    {
        objectElement.GetComponent<Mob>().TakeDamage(_PlayerData.GetAtk(), _directionAngle);
    }

    public void TakeDamage(int Damage, int angle)
    {
        _IsBlinking = true;
        _PlayerData.HitPlayer(Damage);

        /*if (_PlayerData.GetLife() < 1)
        {
            SceneManager.LoadScene("GameOver");
        }*/
        Debug.Log("hit Player : " + m_CharacterLife);
    }
}
