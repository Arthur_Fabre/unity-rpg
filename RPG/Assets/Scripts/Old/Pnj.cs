﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{

	public class Pnj : Npc
	{
        private bool m_move = true;
        private bool m_PlayerInRange = false;

        protected override void Update()
        {
            m_PlayerInRange = false;
            _IsWalking = true;

            _methodToCall = ActionEvent;
            DetectOnLayer(_Players, _methodToCall);
                                   
            if (Input.GetKeyDown(KeyCode.Space) && m_PlayerInRange)
            {
                if (m_DialogBox.GetActive())
                {
                    m_DialogBox.CloseDialog();
                    m_move = true;
                }
                else
                {
                    m_move = false;
                    m_DialogBox.DisplayDialog(m_Dialogue);
                }
            }
            else if (!m_PlayerInRange)
            {
                m_DialogBox.CloseDialog();
                m_move = true;

            }
            else
            {
                m_move = true;
            }

            if (m_IsMovingNpc && m_move && !m_DialogBox.GetActive())
            {
                Move();
            }
            else
            {
                _IsWalking = false;
            }
            _Animator.SetBool("isWalking", _IsWalking);
        }

        protected override void ActionEvent(GameObject objectElement)
        {
            m_PlayerInRange = true;
        }
    }

}