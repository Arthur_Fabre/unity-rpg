﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour
{

    public float m_MoveSpeed = 1f;
    public float m_DetectionRange = 3f;
    public float m_DetectionRadius = 5f;
    public LayerMask m_DetectionLayer;
    public bool m_DebugMode = true;

    public string m_CharacterName = "";
    public int m_CharacterLife = 1;
    public bool m_Invincible;

    protected int _directionAngle;
    protected Vector3 _refVector;
    protected Vector3 _refPoint;

    protected Animator _Animator = null;
    protected bool _IsWalking;

    protected delegate void FuncDelegated(GameObject objectElement);
    protected FuncDelegated _methodToCall;

    protected bool _IsBlinking = false;
    protected int _CurentBlinkingTime = 0;
    protected int _MaxBlinkingTime = 20;
    protected int _BlinkState = 4;


    protected abstract void Awake();
    protected abstract void Update();
    protected abstract void Move();
    protected abstract void ActionEvent(GameObject objectElement);




    protected void DetectOnLayer(GameObject[] ObjectsList, FuncDelegated Action)
    {
        if (ObjectsList.Length > 0)
        {
            foreach (GameObject objectElement in ObjectsList)
            {
                Vector3 inRange = objectElement.transform.position - transform.position;
                if (inRange.sqrMagnitude < m_DetectionRange)
                {
                    Vector3 objectAngle = VectorCoord(objectElement.transform.position, transform.position);
                    float angle = Vector3.Angle(_refVector, objectAngle);
                    if (angle < m_DetectionRadius)
                    {
                        Action(objectElement);
                    }
                }
            }
        }
    }

    protected void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            float range = m_DetectionRange / 2;
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, range);

            Vector3 viewAngleA = DirFromAngle(-m_DetectionRadius, false);
            Vector3 viewAngleB = DirFromAngle(m_DetectionRadius, false);

            Gizmos.DrawLine(transform.position, transform.position + viewAngleA * range);
            Gizmos.DrawLine(transform.position, transform.position + viewAngleB * range);
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position, _refPoint);
        }
    }

    protected Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        Transform tr = GetComponent<Transform>();
        angleInDegrees -= _directionAngle;
        return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

    protected Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    protected void SetDirection(string direction, int angle, Vector3 refPoint)
    {
        _Animator.SetTrigger(direction);
        _directionAngle = angle;
        _refPoint = refPoint;
    }

    protected IEnumerator Sleep(float second)
    {
        yield return new WaitForSeconds(second);
    }

    protected void Blink()
    {
        if (_IsBlinking)
        {
            Debug.Log(_CurentBlinkingTime);
            if(_CurentBlinkingTime <= _MaxBlinkingTime)
            {
                if(_CurentBlinkingTime >= _BlinkState)
                {
                    gameObject.GetComponent<Renderer>().enabled = !gameObject.GetComponent<Renderer>().enabled;
                    _BlinkState += 4;
                }
                _CurentBlinkingTime++;
            }
            else
            {
                _IsBlinking = false;
                _BlinkState = 4;
                _CurentBlinkingTime = 0;
                gameObject.GetComponent<Renderer>().enabled = true;
            }
            
        }
    }
}
