﻿using UnityEngine;
using System.Collections;

public class RespawnEntity : Respawn
{

    public override void RespawnObject()
    {
        Utils.RespawnEntityObject(gameObject);
    }
}
