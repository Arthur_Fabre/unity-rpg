﻿using UnityEngine;
using System.Collections;

public class DestructibleElement : MonoBehaviour
{
    protected Animator _Animator = null;
    public GameObject m_SummonItem;

    void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_Animator.GetCurrentAnimatorStateInfo(0).IsName("Broken"))
        {
            if (m_SummonItem != null)
            {
                m_SummonItem.transform.position = transform.position;
                Instantiate(m_SummonItem);
            }
            Utils.killEntity(gameObject);
        }
    }
}
