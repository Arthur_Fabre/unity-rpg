﻿using UnityEngine;
using System.Collections;

public class RespawnMob : Respawn
{

    public override void RespawnObject()
    {
        Debug.Log(gameObject.GetComponent<NPCController>() != null);
        Debug.Log(gameObject.GetComponent<NPCController>().Kill);
        Debug.Log(gameObject.GetComponent<NPCController>().GetNpcData().IsRespawnable);
        if (gameObject.GetComponent<NPCController>() != null && gameObject.GetComponent<NPCController>().Kill && gameObject.GetComponent<NPCController>().GetNpcData().IsRespawnable)
        {
            gameObject.transform.position = gameObject.GetComponent<NPCController>().GetNpcData().InitPosition;
            gameObject.GetComponent<NPCController>().GetNpcData().SetData();
            gameObject.GetComponent<NPCController>().Kill = false;
            Utils.RespawnEntityObject(gameObject);
        }
    }
}
