﻿using UnityEngine;
using System.Collections;

public abstract class Respawn : MonoBehaviour
{
    public abstract void RespawnObject();
}
