﻿using UnityEngine;
using System.Collections;

public class CollectibleElement : MonoBehaviour
{
    public PlayerData m_PlayerData;
    // Use this for initialization

    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.tag);
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject, .1f);
            if (gameObject.CompareTag("Coin")) m_PlayerData.AddMoney();
            else if (gameObject.CompareTag("Heart")) m_PlayerData.AddLife();
            else if (gameObject.CompareTag("HeartPart")) m_PlayerData.AddMaxLife();

        }
    }
}
