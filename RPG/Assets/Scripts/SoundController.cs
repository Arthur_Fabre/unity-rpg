﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour
{

    [SerializeField] private GameSettings m_GameSettings;

    // Update is called once per frame
    void Update()
    {
        if (gameObject.tag == "AmbiantSound")
        {
            gameObject.GetComponent<AudioSource>().volume = m_GameSettings.VolumeAmbientSound;
            gameObject.GetComponent<AudioSource>().enabled = m_GameSettings.PlayAmbientSound;
        }
        else if(gameObject.tag == "InteractSound")
        {
            gameObject.GetComponent<AudioSource>().volume = m_GameSettings.VolumeInteractSound;
            gameObject.GetComponent<AudioSource>().enabled = m_GameSettings.PlayInteractSound;
        }
    }
}
