﻿using UnityEngine;
using System.Collections;

public abstract class Trigger : MonoBehaviour
{

    public GameObject m_Entity = null;
    public AudioSource m_AudioSource;
    protected bool _IsTrigger = false;

    // Update is called once per frame
    protected abstract void Update();

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(m_Entity.tag))
        {
            _IsTrigger = true;
        }
    }

    protected abstract void OnTriggerExit2D(Collider2D other);
}
