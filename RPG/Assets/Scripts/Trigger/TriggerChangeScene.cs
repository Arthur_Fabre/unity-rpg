﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TriggerChangeScene : Trigger
{

    // Ajouter une boite de dialogue c'est fermer
    [SerializeField]
    private string LevelToLoad;
    [SerializeField]
    private bool m_WithAction;

    protected override void Update()
    {
        if (_IsTrigger)
        {
            if (m_WithAction)
            {
                m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Interact;
                if (Input.GetKeyDown(KeyCode.LeftControl))
                {
                    SceneManager.LoadScene(LevelToLoad);
                }
            }
            else
            {
                SceneManager.LoadScene(LevelToLoad);
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _IsTrigger = false;
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
        }
    }
}
