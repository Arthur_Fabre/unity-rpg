﻿using UnityEngine;
using System.Collections;

public class TriggerReloadRegion : MonoBehaviour
{

    [SerializeField] private GameObject m_Entity = null;
    [SerializeField] private GameObject m_Region = null;

    protected void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(m_Entity.tag))
        {
            for (int i = 0; i < m_Region.transform.Find("Mobs").gameObject.transform.childCount; i++)
            {
                m_Region.transform.Find("Mobs").gameObject.transform.GetChild(i).GetComponent<Respawn>().RespawnObject();
            }

            for (int i = 0; i < m_Region.transform.Find("Entity").gameObject.transform.childCount; i++)
            {
                m_Region.transform.Find("Entity").gameObject.transform.GetChild(i).GetComponent<Respawn>().RespawnObject();
            }
        }
    }
}
