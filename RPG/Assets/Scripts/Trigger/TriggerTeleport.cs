﻿using UnityEngine;
using System.Collections;

public class TriggerTeleport : Trigger
{

    [SerializeField]
    private GameObject m_TpPos;
    [SerializeField]
    private bool m_WithAction;

    protected override void Update()
    {
        if (_IsTrigger)
        {
            if (m_WithAction)
            {
                m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Interact;
                if (Input.GetKeyDown(KeyCode.LeftControl))
                {
                    m_Entity.transform.position = m_TpPos.transform.position;
                }
            }
            else
            {
                m_Entity.transform.position = m_TpPos.transform.position;
            }

        }

    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(m_Entity.tag))
        {
            _IsTrigger = false;
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
        }
    }
}
