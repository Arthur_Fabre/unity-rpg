﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class PlaceTrigger : Trigger
{
    [SerializeField] private PlaceBox m_PlaceBox = null;
    [SerializeField] private PlaceData m_Region;
    [SerializeField] private bool m_Display = true;
    [SerializeField] private GameObject m_RegionObject = null;

    protected override void Update()
    { }

    public bool UpdateDisplay()
    {
        bool display = false;
        if (_IsTrigger)
        {
            if (m_Entity.GetComponent<PlayerController>().GetPlayerData().Place != m_Region)
            {
                display = true;
                ReloadRegion();
                m_Entity.GetComponent<PlayerController>().GetPlayerData().Place = m_Region;
                m_AudioSource.clip = m_Region.PlaceSound;
                m_AudioSource.Play();

            }
            
        }

        return display;

    }

    public void ShowDisplay()
    {
        if (m_Display) m_PlaceBox.DisplayBox(m_Region);
    }

    public void HideDisplay()
    {
        if (m_Display) m_PlaceBox.CloseBox();
    }

    private void ReloadRegion()
    {
        Debug.Log("Mob count "+ m_RegionObject.transform.Find("Mobs").gameObject.transform.childCount);
        for (int i = 0; i < m_RegionObject.transform.Find("Mobs").gameObject.transform.childCount; i++)
        {
            m_RegionObject.transform.Find("Mobs").gameObject.transform.GetChild(i).GetComponent<Respawn>().RespawnObject();
        }
        Debug.Log("Entity count " + m_RegionObject.transform.Find("Entity").gameObject.transform.childCount);
        for (int i = 0; i < m_RegionObject.transform.Find("Entity").gameObject.transform.childCount; i++)
        {
            if (m_RegionObject.transform.Find("Entity").gameObject.transform.GetChild(i).GetComponent<Respawn>() != null)
            {
                m_RegionObject.transform.Find("Entity").gameObject.transform.GetChild(i).GetComponent<Respawn>().RespawnObject();
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(m_Entity.tag))
        {
            _IsTrigger = false;
        }
    }
}
