﻿using UnityEngine;
using System.Collections;

public class TriggerTeleportWithAnimator : Trigger
{

    [SerializeField]
    private GameObject m_TpPos;
    protected Animator _Animator = null;

    void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    protected override void Update()
    {
        if (_IsTrigger)
        {
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Interact;
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                _Animator.SetTrigger("Open");
            }

            if (_Animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                m_Entity.transform.position = m_TpPos.transform.position;
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag(m_Entity.tag))
        {
            _IsTrigger = false;
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
        }
    }
}
