﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TriggerChangeSceneWithAnimator : Trigger
{

    // Ajouter une boite de dialogue c'est fermer
    [SerializeField]
    private string LevelToLoad;
    protected Animator _Animator = null;

    void Awake()
    {
        _Animator = GetComponent<Animator>();
    }

    protected override void Update()
    {
        if (_IsTrigger)
        {
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Interact;
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                _Animator.SetTrigger("Open");
            }

            if (_Animator.GetCurrentAnimatorStateInfo(0).IsName("Open"))
            {
                SceneManager.LoadScene(LevelToLoad);
            }
        }
    }

    protected override void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            _IsTrigger = false;
            m_Entity.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
        }
    }
}
