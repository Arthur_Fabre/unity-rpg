﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour
{

    public bool IsBlinking = false;
    [SerializeField]
    private AudioSource audioData;

    private int _CurentBlinkingTime = 0;
    private int _MaxBlinkingTime = 20;
    private int _BlinkState = 4;

    public void BlinkUpdate()
    {
        if (IsBlinking)
        {
            Debug.Log(_CurentBlinkingTime);
            if (_CurentBlinkingTime <= _MaxBlinkingTime)
            {
                if (_CurentBlinkingTime >= _BlinkState)
                {
                    gameObject.GetComponent<Renderer>().enabled = !gameObject.GetComponent<Renderer>().enabled;
                    _BlinkState += 4;
                }
                _CurentBlinkingTime++;
            }
            else
            {
                IsBlinking = false;
                _BlinkState = 4;
                _CurentBlinkingTime = 0;
                gameObject.GetComponent<Renderer>().enabled = true;
            }

        }
    }
}
