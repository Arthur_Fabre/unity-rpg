﻿using UnityEngine;
using System.Collections;

public class FightPlayer : MonoBehaviour
{
    [SerializeField]    private PlayerData m_PlayerData;
    [SerializeField]    private bool m_DebugMode = true;

    [SerializeField]    private float m_HitRange = 3f;
    [SerializeField]    private float m_HitRadius = 45f;
    [SerializeField]    private AudioSource m_AudioSource;
    [SerializeField]    private GameSoundList m_GameState;

    private GameObject[] m_Pots = null;
    private GameObject[] m_Enemies = null;


    public bool DetectInputAttackAction1()
    {
        return Input.GetKey(KeyCode.W);
    }
    public bool DetectInputAttackAction2()
    {
        return Input.GetKey(KeyCode.X);
    }

    public void PlayerProtected(bool state)
    {
        if (state)
        {
            m_AudioSource.clip = m_GameState.Protect;
            m_AudioSource.Play();
        }
        m_PlayerData.IsProtected = state;
    }


    public bool HitEntityInRange()
    {
        m_AudioSource.clip = m_GameState.Attack;
        m_AudioSource.Play();
        m_Pots = GameObject.FindGameObjectsWithTag("Pot");
        m_Enemies = GameObject.FindGameObjectsWithTag("Enemy");

        bool detect = CheckInRange(m_Pots);
        return detect ? detect : CheckInRange(m_Enemies);
    }

    private bool CheckInRange(GameObject[] ObjectsList)
    {
        bool isInRange = false;
        foreach (GameObject objectElement in ObjectsList)
        {
            if (Vector3.Distance(objectElement.transform.position, transform.position) <= m_HitRange)
            {
                Vector3 CurrentToTargetVector = VectorCoord(objectElement.transform.transform.position, transform.position);
                float angle = Vector3.Angle(gameObject.GetComponent<PlayerController>().RefVector, CurrentToTargetVector);
                if (angle < m_HitRadius)
                {
                    isInRange = true;
                    if (objectElement.CompareTag("Pot"))
                    {
                        objectElement.GetComponent<Animator>().SetTrigger("Broke");
                    }
                    else if (objectElement.CompareTag("Enemy"))
                    {
                        Debug.Log("Hit enemy");
                        if (!objectElement.GetComponent<Blink>().IsBlinking)
                        {
                            objectElement.GetComponent<NPCController>().GetNpcData().HitNpc(m_PlayerData.GetAtk());
                            objectElement.GetComponent<Blink>().IsBlinking = true;
                        }
                    }
                }
            }
        }
        return isInRange;
    }

    private void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            float range = m_HitRange;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, range);

            Vector3 viewAngleA = DirFromAngle(-m_HitRadius);
            Vector3 viewAngleB = DirFromAngle(m_HitRadius);

            Gizmos.DrawLine(transform.position, transform.position + viewAngleA * range);
            Gizmos.DrawLine(transform.position, transform.position + viewAngleB * range);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position, gameObject.GetComponent<PlayerController>().RefPoint);
        }
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    private Vector3 DirFromAngle(float angleInDegrees)
    {
        Vector3 vectorToTop = transform.position;
        vectorToTop.y -= m_HitRange;
        float angle = Vector3.Angle(gameObject.GetComponent<PlayerController>().RefVector, VectorCoord(transform.position, vectorToTop));

        if (gameObject.GetComponent<PlayerController>().RefVector.x < 0)
            angle = -angle;

        Transform tr = GetComponent<Transform>();
        angleInDegrees -= angle;
        return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

}
