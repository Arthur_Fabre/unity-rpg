﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

//Npc Direction
// 1 : Up
// 2 : Left
// 3 : Down
// 4 : Right

public class PlayerController : MonoBehaviour
{
    [SerializeField] private int m_Direction = 1;
    [SerializeField] private PlayerData m_PlayerData = null;
    [SerializeField] private GameSettings m_GameSettings;

    private Vector3 _RefVector;
    private Vector3 _RefPoint;

    private bool _Kill = false;

    public int Direction { get; set; }
    public Vector3 RefVector { get; set; }
    public Vector3 RefPoint { get; set; }


    public PlayerData GetPlayerData()
    {
        return m_PlayerData;
    }

    public void Awake()
    {
        transform.position = m_PlayerData.position;
    }


    public void Update()
    {
        if (m_PlayerData.Life < 1 && !_Kill)
        {
            m_GameSettings.save = false;
            _Kill = true;
            SceneManager.LoadScene("GameOver");
        }
    }

}
