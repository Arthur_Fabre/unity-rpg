﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Input : StateMachineBehaviour
{
    // OnStateEnter is called before OnStateEnter is called on any state inside this state machine
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsWalking", animator.GetComponent<MovePlayer>().DetectInputMove());
        animator.SetBool("IsAttack", false);
        animator.SetInteger("Direction", animator.GetComponent<PlayerController>().Direction);
        UIItemElements Action1Equipment = animator.GetComponent<PlayerController>().GetPlayerData().Inventory.Action1Equipment;
        UIItemElements Action2Equipment = animator.GetComponent<PlayerController>().GetPlayerData().Inventory.Action2Equipment;
        if (Action1Equipment == UIItemElements.Sword)       animator.SetBool("IsAttack", animator.GetComponent<FightPlayer>().DetectInputAttackAction1());
        else if(Action2Equipment == UIItemElements.Sword)   animator.SetBool("IsAttack", animator.GetComponent<FightPlayer>().DetectInputAttackAction2());

        if (Action1Equipment == UIItemElements.Shield) animator.SetBool("IsProtect", animator.GetComponent<FightPlayer>().DetectInputAttackAction1());
        else if (Action2Equipment == UIItemElements.Shield) animator.SetBool("IsProtect", animator.GetComponent<FightPlayer>().DetectInputAttackAction2());
    }

    // OnStateExit is called before OnStateExit is called on any state inside this state machine
    //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMove is called before OnStateMove is called on any state inside this state machine
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateIK is called before OnStateIK is called on any state inside this state machine
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateMachineEnter is called when entering a state machine via its Entry Node
    //override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}

    // OnStateMachineExit is called when exiting a state machine via its Exit Node
    //override public void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    //{
    //    
    //}
}
