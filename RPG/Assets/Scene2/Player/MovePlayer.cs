﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour
{

    [SerializeField]    private float m_MouvementSpeed = 3f;
    [SerializeField]    private AudioSource m_AudioSource;
    [SerializeField]    private GameSoundList m_GameState;

    private bool _IsWalking;
    private int _run = 0;

    public bool IsWalking { get; }

    public void PlaySound()
    {
        m_AudioSource.clip = m_GameState.Walk;
        m_AudioSource.loop = true;
        m_AudioSource.pitch = 0.8f;
        m_AudioSource.Play();
    }

    public void StopSound()
    {
        m_AudioSource.Stop();
        m_AudioSource.loop = false;
        m_AudioSource.pitch = 1f;
    }

    public bool DetectInputMove()
    {
        _IsWalking = true;
        if (Input.GetKey(KeyCode.DownArrow)) ChangeDir(3);
        else if (Input.GetKey(KeyCode.UpArrow)) ChangeDir(1);
        else if (Input.GetKey(KeyCode.LeftArrow)) ChangeDir(2);
        else if (Input.GetKey(KeyCode.RightArrow)) ChangeDir(4);
        else _IsWalking = false;

        _run = 0;
        if (Input.GetKey(KeyCode.LeftControl)) _run = 2;

        return _IsWalking;
    }


    private void ChangeDir(int dir)
    {
        gameObject.GetComponent<PlayerController>().Direction = dir;
        Vector3 newRef = transform.position;
        switch (gameObject.GetComponent<PlayerController>().Direction)
        {
            case 1:
                newRef.y += 2;
                break;
            case 2:
                newRef.x -= 2;
                break;
            case 3:
                newRef.y -= 2;
                break;
            case 4:
                newRef.x += 2;
                break;
        }
        gameObject.GetComponent<PlayerController>().RefPoint = newRef;
        gameObject.GetComponent<PlayerController>().RefVector = VectorCoord(newRef, transform.position);

    }

    public void UpdateMove()
    {
        Vector3 mouvement = Vector3.zero;
            mouvement.x = Input.GetAxisRaw("Horizontal");
            mouvement.y = Input.GetAxisRaw("Vertical");
        mouvement.Normalize();

        gameObject.GetComponent<PlayerController>().RefPoint += mouvement * (m_MouvementSpeed + _run) * Time.deltaTime;
        gameObject.GetComponent<PlayerController>().RefVector = VectorCoord(gameObject.GetComponent<PlayerController>().RefPoint, transform.position);
        transform.position += mouvement * (m_MouvementSpeed + _run) * Time.deltaTime;
        gameObject.GetComponent<PlayerController>().GetPlayerData().position = transform.position;
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    public int GetDirection()
    {
        return gameObject.GetComponent<PlayerController>().Direction;
    }
}
