﻿using UnityEngine;
using System.Collections;

public class DetectEntity : MonoBehaviour
{

    [SerializeField]
    private float m_DetectionRange = 3f;
    [SerializeField]
    private float m_DetectionRadius = 45f;
    [SerializeField]
    private bool m_DebugMode = true;

    private GameObject[] m_Pots = null;
    private GameObject[] m_Enemies = null;


    public bool IsEntityInRange()
    {
        m_Pots = GameObject.FindGameObjectsWithTag("Pot");
        m_Enemies = GameObject.FindGameObjectsWithTag("Enemy");

        bool detect = CheckInRange(m_Pots);
        return detect ? detect : CheckInRange(m_Enemies);
    }

    private bool CheckInRange(GameObject[] ObjectsList)
    {
        bool isInRange = false;
        foreach (GameObject objectElement in ObjectsList)
        {
            if (Vector3.Distance(objectElement.transform.position, transform.position) <= m_DetectionRange)
            {
                Vector3 CurrentToTargetVector = VectorCoord(objectElement.transform.transform.position, transform.position);
                float angle = Vector3.Angle(gameObject.GetComponent<PlayerController>().RefVector, CurrentToTargetVector);
                if (angle < m_DetectionRadius)
                {
                    isInRange = true;
                }
            }
        }
        return isInRange;
    }

    private void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            Debug.Log("Detect : " + gameObject.GetComponent<PlayerController>().RefPoint);
            float range = m_DetectionRange;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, range);

            Vector3 viewAngleA = DirFromAngle(-m_DetectionRadius);
            Vector3 viewAngleB = DirFromAngle(m_DetectionRadius);

            Gizmos.DrawLine(transform.position, transform.position + viewAngleA * range);
            Gizmos.DrawLine(transform.position, transform.position + viewAngleB * range);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position, gameObject.GetComponent<PlayerController>().RefPoint);
        }
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    private Vector3 DirFromAngle(float angleInDegrees)
    {
        Vector3 vectorToTop = transform.position;
        vectorToTop.y -= m_DetectionRange;
        float angle = Vector3.Angle(gameObject.GetComponent<PlayerController>().RefVector, VectorCoord(transform.position, vectorToTop));

        if (gameObject.GetComponent<PlayerController>().RefVector.x < 0)
            angle = -angle;

        Transform tr = GetComponent<Transform>();
        angleInDegrees -= angle;
        return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}
