﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    [SerializeField]
    private float m_MouvementSpeed = 3f;
    [SerializeField]
    private int m_MinTimeBeforeChange = 50;
    [SerializeField]
    private int m_MaxTimeBeforeChange = 100;

    [SerializeField]
    private int _ChangeDirValue = 50;
    private int _CurentValue = 0;


    private int Rand()
    {
        return Random.Range(m_MinTimeBeforeChange, m_MaxTimeBeforeChange);
    }

    private void ChangeDir()
    {
        _ChangeDirValue = Rand();
        _CurentValue = 0;
        gameObject.GetComponent<NPCController>().Direction = Random.Range(1, 5);
        Vector3 newRef = transform.position;
        switch (gameObject.GetComponent<NPCController>().Direction)
        {
            case 1:
                newRef.y += 2; 
                break;
            case 2:
                newRef.x -= 2;
                break;
            case 3:
                newRef.y -= 2;
                break;
            case 4:
                newRef.x += 2;
                break;
        }
        gameObject.GetComponent<NPCController>().RefPoint = newRef;
        gameObject.GetComponent<NPCController>().RefVector = VectorCoord(newRef, transform.position);

    }

    public  void UpdateMove()
    {
        if (_CurentValue > _ChangeDirValue) ChangeDir();
        Vector3 mouvement = Vector3.zero;

        if (gameObject.GetComponent<NPCController>().Direction == 1) mouvement.y = -transform.position.y;
        else if (gameObject.GetComponent<NPCController>().Direction == 3) mouvement.y = +transform.position.y;
        else if (gameObject.GetComponent<NPCController>().Direction == 2) mouvement.x = +transform.position.x;
        else if (gameObject.GetComponent<NPCController>().Direction == 4) mouvement.x = -transform.position.x;


        mouvement.Normalize();
        gameObject.GetComponent<NPCController>().RefPoint += mouvement * m_MouvementSpeed * Time.deltaTime;
        gameObject.GetComponent<NPCController>().RefVector = VectorCoord(gameObject.GetComponent<NPCController>().RefPoint, transform.position);
        transform.position += mouvement * m_MouvementSpeed * Time.deltaTime;
        _CurentValue++;
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

}
