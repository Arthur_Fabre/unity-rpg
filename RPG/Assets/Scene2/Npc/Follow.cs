﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follow : MonoBehaviour
{
    [SerializeField]
    private Transform m_FollowObject = null;
    [SerializeField]
    private float m_Speed = 3f;
    [SerializeField]
    private float m_ArrivalDistance = 0.5f;

    public void UpdateFollow()
    {
        if(Vector3.Distance(transform.position,m_FollowObject.position)> m_ArrivalDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, m_FollowObject.position, m_Speed * Time.deltaTime);
            gameObject.GetComponent<NPCController>().RefPoint = m_FollowObject.position;
            gameObject.GetComponent<NPCController>().RefVector = VectorCoord(m_FollowObject.position, transform.position);
            int newDirection = CalcDirection(transform.position, m_FollowObject.position);

            if (gameObject.GetComponent<NPCController>().Direction != newDirection)
            {
                gameObject.GetComponent<NPCController>().Direction = newDirection;
                Debug.Log("Direction change " + newDirection);
            }
            
        }
    }

    public int GetDirection()
    {
        return gameObject.GetComponent<NPCController>().Direction;
    }


    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    private int CalcDirection(Vector3 position, Vector3 targetPosition)
    {
        int direction = 1;
        float diffY = position.y - targetPosition.y;
        float diffX = position.x - targetPosition.x;

        if (diffY < 0)  direction = 1;
        else            direction = 3;
        if (diffX < 0)  direction = Mathf.Abs(diffX) > Mathf.Abs(diffY) ? 4 : direction;
        else            direction = Mathf.Abs(diffX) > Mathf.Abs(diffY) ? 2 : direction;

        return direction;
    }
}
