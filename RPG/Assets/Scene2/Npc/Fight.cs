﻿using UnityEngine;
using System.Collections;

public class Fight : MonoBehaviour
{

    [SerializeField]
    private float m_HitRange = 1f;
    [SerializeField]
    private GameObject m_TargetObject = null;
    [SerializeField]
    private int m_HitDamage = 1;
    [SerializeField]
    private bool m_DebugMode = true;

    public bool IsTargetInRange()
    {
        return Vector3.Distance(m_TargetObject.transform.position, transform.position) <= m_HitRange;
    }

    public void HitTarget()
    {
        if (!m_TargetObject.GetComponent<Blink>().IsBlinking)
        {
            if (!m_TargetObject.GetComponent<PlayerController>().GetPlayerData().IsProtected)
            {
                m_TargetObject.GetComponent<PlayerController>().GetPlayerData().HitPlayer(m_HitDamage);
                m_TargetObject.GetComponent<Blink>().IsBlinking = true;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            float range = m_HitRange/2;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }
}
