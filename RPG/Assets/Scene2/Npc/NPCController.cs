﻿using UnityEngine;
using System.Collections;

//Npc Direction
// 1 : Up
// 2 : Left
// 3 : Down
// 4 : Right

public class NPCController : MonoBehaviour
{
    [SerializeField] private int m_Direction = 1;
    [SerializeField] private NpcData m_NpcData = null;
    [SerializeField] private GameObject m_SummonItem;
    public bool Kill = false;

    private Vector3 _RefVector;
    private Vector3 _RefPoint;


    public int Direction { get; set; }
    public Vector3 RefVector { get; set; }
    public Vector3 RefPoint { get; set; }


    public NpcData GetNpcData()
    {
        return m_NpcData;
    }

    public void Awake()
    {
        GetComponent<Animator>().SetInteger("Direction", Direction);
    }

    public void Update()
    {
        //Debug.Log("Life : " + m_NpcData.Life);
        //BUG
        if (m_NpcData.Life < 1 && !Kill && !m_NpcData.IsInvincible)
        {
            Kill = true;
            m_SummonItem.transform.position = transform.position;
            Utils.killEntity(gameObject);
            Instantiate(m_SummonItem);
        }
    }

}
