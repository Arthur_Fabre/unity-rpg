﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectPlayer : MonoBehaviour
{
    [SerializeField] private float m_DetectionRange = 5f;
    [SerializeField] private float m_DetectionRadius = 45f;
    [SerializeField] private Transform m_TargetObject = null;
    [SerializeField] private bool m_DebugMode = true;

    public bool IsCharacterInRange()
    {
        bool isInRange = false;
        if (Vector3.Distance(m_TargetObject.position, transform.position) <= m_DetectionRange)
        {
            Vector3 CurrentToTargetVector = VectorCoord(m_TargetObject.transform.position, transform.position);
            float angle = Vector3.Angle(gameObject.GetComponent<NPCController>().RefVector, CurrentToTargetVector);
            if (angle < m_DetectionRadius)
            {
                isInRange = true;
            }
        }
        return isInRange;
    }

    private void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            float range = m_DetectionRange/2;
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(transform.position, range);

            Vector3 viewAngleA = DirFromAngle(-m_DetectionRadius);
            Vector3 viewAngleB = DirFromAngle(m_DetectionRadius);

            Gizmos.DrawLine(transform.position, transform.position + viewAngleA * range);
            Gizmos.DrawLine(transform.position, transform.position + viewAngleB * range);

            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(transform.position, gameObject.GetComponent<NPCController>().RefPoint);
        }
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    private Vector3 DirFromAngle(float angleInDegrees)
    {
        Vector3 vectorToTop = transform.position;
        vectorToTop.y -= m_DetectionRange;
        float angle = Vector3.Angle(gameObject.GetComponent<NPCController>().RefVector, VectorCoord(transform.position, vectorToTop));

        if (gameObject.GetComponent<NPCController>().RefVector.x < 0)
            angle = -angle;

        Transform tr = GetComponent<Transform>();
        angleInDegrees -= angle;
        return new Vector2(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }

}
