﻿using UnityEngine;
using System.Collections;

public class Chat : MonoBehaviour
{

    [SerializeField] private float m_ChatRange = 1f;
    [SerializeField] private GameObject m_TargetObject = null;
    [SerializeField] private DialogueData m_Dialogue = null;
    [SerializeField] private DialogBox m_DialogBox = null;
    [SerializeField] private bool m_DebugMode = true;

    private bool _IsInRange = false;

    public bool IsCharacterInRange()
    {
        _IsInRange = Vector3.Distance(m_TargetObject.transform.position, transform.position) <= m_ChatRange;
        if (!_IsInRange)
        {
            m_TargetObject.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
            m_DialogBox.CloseDialog();
        }

        return _IsInRange;
    }

    public void FollowPlayer()
    {
            gameObject.GetComponent<NPCController>().RefPoint = m_TargetObject.transform.position;
            gameObject.GetComponent<NPCController>().RefVector = VectorCoord(m_TargetObject.transform.position, transform.position);
            int newDirection = CalcDirection(transform.position, m_TargetObject.transform.position);

            if (gameObject.GetComponent<NPCController>().Direction != newDirection)
            {
                gameObject.GetComponent<NPCController>().Direction = newDirection;
            }

    }

    public void UpdateChat()
    {
        if (_IsInRange)
        {
            if(!m_DialogBox.GetActive()) m_TargetObject.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Speak;
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                if (m_DialogBox.GetActive())
                {
                    m_DialogBox.CloseDialog();
                }
                else
                {
                    m_TargetObject.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Quit;
                    m_DialogBox.DisplayDialog(m_Dialogue);
                }
            }
        }
        else
        {
            m_TargetObject.GetComponent<PlayerController>().GetPlayerData().Inventory.Action3Interact = UIKeyAction3.Run;
            m_DialogBox.CloseDialog();
        }
    }


    private void OnDrawGizmos()
    {
        if (m_DebugMode)
        {
            float range = m_ChatRange/2;
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(transform.position, range);
        }
    }

    private Vector3 VectorCoord(Vector3 A, Vector3 B)
    {
        return new Vector3(B.x - A.x, B.y - A.y, B.z - A.z);
    }

    private int CalcDirection(Vector3 position, Vector3 targetPosition)
    {
        int direction = 1;
        float diffY = position.y - targetPosition.y;
        float diffX = position.x - targetPosition.x;

        if (diffY < 0) direction = 1;
        else direction = 3;
        if (diffX < 0) direction = Mathf.Abs(diffX) > Mathf.Abs(diffY) ? 4 : direction;
        else direction = Mathf.Abs(diffX) > Mathf.Abs(diffY) ? 2 : direction;

        return direction;
    }
}
